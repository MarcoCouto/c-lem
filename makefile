LST=stmts/

OUTFILE=
NTIMES=
MTIMES=

ifeq ($(DEFS), )
	MACROS=
else
	MACROS=-D $(DEFS)
endif

ifeq ($(INPUTS), )
	INS=
else
	INS= < $(INPUTS)
endif


main: main.c rapl.o lst
	gcc -Wall $(MACROS) -o main main.c rapl.o $(LST)*.o -lm > /dev/null 2>&1
	chmod 777 main

run: 
	sudo modprobe msr
	sudo ./main $(OUTFILE) $(NTIMES) $(MTIMES) $(INS) > /dev/null 2>&1

mainJava: mainJava.c rapl.o
	gcc -O2 -Wall -o mainJava mainJava.c rapl.o -lm > /dev/null 2>&1
	chmod 777 main

rapl.o: rapl.c rapl.h
	gcc -O2 -Wall -c rapl.c  -lm > /dev/null 2>&1

lst: $(LST)*.c
	cd $(LST) && gcc -c *.c -lm > /dev/null 2>&1

clean:
	find . -type f -name "*.o" | xargs rm -f

#python model_generator.py > /dev/null 2>&1 ; sleep(60) ; python examples/dnMeasure.py > /dev/null 2>&1
