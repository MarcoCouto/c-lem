	.file	"funCall.c"
	.text
	.globl	ff
	.type	ff, @function
ff:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	addl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	ff, .-ff
	.section	.rodata
.LC0:
	.string	"%d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$192, %rsp
	movw	$25637, -192(%rbp)
	leaq	-176(%rbp), %rsi
	movl	$0, %eax
	movl	$22, %edx
	movq	%rsi, %rdi
	movq	%rdx, %rcx
	rep stosq
	movl	$1353609359, -176(%rbp)
	movl	$1191706187, -172(%rbp)
	movl	$1138383479, -168(%rbp)
	movl	$-786173523, -164(%rbp)
	movl	$1428201890, -160(%rbp)
	movl	$-1703625709, -156(%rbp)
	movl	$1877816847, -152(%rbp)
	movl	$-122718102, -148(%rbp)
	movl	$957748280, -144(%rbp)
	movl	$1045144934, -140(%rbp)
	movl	$-1585104497, -136(%rbp)
	movl	$-1897248522, -132(%rbp)
	movl	$1930145946, -128(%rbp)
	movl	$-1110420768, -124(%rbp)
	movl	$1086512010, -120(%rbp)
	movl	$-180133721, -116(%rbp)
	movl	$1986399749, -112(%rbp)
	movl	$331454678, -108(%rbp)
	movl	$2129034993, -104(%rbp)
	movl	$-1437080792, -100(%rbp)
	movl	$1684444141, -96(%rbp)
	movl	$-2106466167, -92(%rbp)
	movl	$-1873859879, -88(%rbp)
	movl	$1696593888, -84(%rbp)
	movl	$1196106671, -80(%rbp)
	movl	$-38027103, -76(%rbp)
	movl	$1049092288, -72(%rbp)
	movl	$1664848723, -68(%rbp)
	movl	$-1364948192, -64(%rbp)
	movl	$609450207, -60(%rbp)
	movl	$-1572406111, -56(%rbp)
	movl	$-593687679, -52(%rbp)
	movl	$1309955412, -48(%rbp)
	movl	$1556578893, -44(%rbp)
	movl	$-1360758606, -40(%rbp)
	movl	$-1597006991, -36(%rbp)
	movl	$1101404822, -32(%rbp)
	movl	$-948535941, -28(%rbp)
	movl	$-1533257009, -24(%rbp)
	movl	$756990518, -20(%rbp)
	movl	$-1822474939, -16(%rbp)
	movl	$-531586478, -12(%rbp)
	movl	$1299008901, -8(%rbp)
	leaq	-176(%rbp), %rdx
	leaq	-192(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_scanf
	movl	%eax, -180(%rbp)
	movl	-180(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	-180(%rbp), %eax
	movl	%eax, %edi
	call	ff
	movl	%eax, -180(%rbp)
	movl	-180(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 4.8.4-2ubuntu1~14.04.3) 4.8.4"
	.section	.note.GNU-stack,"",@progbits
